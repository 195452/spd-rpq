#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <list>
#include <unordered_set>

using namespace std;

struct Task
{
    size_t r,p,q,id;
    bool operator < (const Task& str) const
    {
        return ( r  < str.r );
    }
    void operator = (const Task& str)
    {
        r=str.r;
        p=str.p;
        q=str.q;
        id=str.id;
    }
};

struct less_than_r
{
    inline bool operator() (const Task& str1, const Task& str2)
    {
        return (str1.r < str2.r);
    }
};
struct less_than_q
{
    inline bool operator() (const Task& str1, const Task& str2)
    {
        return (str1.q < str2.q);
    }
};

void algorytm_rpq(vector <Task> & zadania_r)

{
    sort(zadania_r.begin(),zadania_r.end(),less_than_r());
    list <Task> zadania_lista_r(zadania_r.begin(),zadania_r.end()); //posortowana od najmniejszego r

    sort(zadania_r.begin(),zadania_r.end(),less_than_q());
    list <Task> zadania_lista_q(zadania_r.begin(),zadania_r.end()); //posortowana od najmniejszego q

    list <Task> zadania_lista_out; //lista ulozona

    unordered_set<int> nieulozone_zadania; //do sledzenia ktore zadania zostaly juz ulozone

    for (size_t i=0; i<zadania_r.size(); i++)
        nieulozone_zadania.insert(i);

    list <Task>::iterator iterator_r; //iterator dla 'gornej' czesci listy
    list <Task>::iterator iterator_q; //iterator dla 'dolnej' czesci listy

    //wrzucenie pierszych dwoch elementow (na gore i na dol)
    nieulozone_zadania.erase((*zadania_lista_r.begin()).id);
    nieulozone_zadania.erase((*zadania_lista_q.begin()).id);
    zadania_lista_out.splice(zadania_lista_out.begin(),zadania_lista_r,zadania_lista_r.begin());
    zadania_lista_out.splice(zadania_lista_out.end(),zadania_lista_q,zadania_lista_q.begin());

    iterator_r=zadania_lista_out.begin();
    iterator_q=--(zadania_lista_out.end());

    while (zadania_lista_out.size()!=zadania_r.size())
    {
        while(nieulozone_zadania.find((*zadania_lista_r.begin()).id)==nieulozone_zadania.end() && !zadania_lista_r.empty()) // dopkoki element na liscie r powinien zostac usuniety (bo zostal usuniety z listy q)
            zadania_lista_r.pop_front(); //czyszczenie usunietych wszesniej (z drugiej listy)
        while(nieulozone_zadania.find((*zadania_lista_q.begin()).id)==nieulozone_zadania.end() && !zadania_lista_q.empty()) // dopkoki element na liscie r powinien zostac usuniety (bo zostal usuniety z listy q)
            zadania_lista_q.pop_front(); //czyszczenie usunietych wszesniej (z drugiej listy)
        if (!zadania_lista_r.empty())
        {   //wstawiam na gorna czesc listy
            nieulozone_zadania.erase((*zadania_lista_r.begin()).id);
            zadania_lista_out.splice(++(iterator_r),zadania_lista_r,zadania_lista_r.begin());
            iterator_r--;
            while(nieulozone_zadania.find((*zadania_lista_q.begin()).id)==nieulozone_zadania.end() && !zadania_lista_q.empty()) // dopkoki element na liscie r powinien zostac usuniety (bo zostal usuniety z listy q)
                zadania_lista_q.pop_front(); //czyszczenie usunietych wszesniej (z drugiej listy)
        }
        if (!zadania_lista_q.empty())
        {   //wstawiam na dolna czesc listy
            nieulozone_zadania.erase((*zadania_lista_q.begin()).id);
            zadania_lista_out.splice(iterator_q,zadania_lista_q,zadania_lista_q.begin());
            iterator_q--;
        }
    }
    zadania_r.clear();
    copy(zadania_lista_out.begin() ,zadania_lista_out.end(), back_inserter(zadania_r) );
}

int main()
{
    size_t i, j;
    size_t czas_suma=0;
    ifstream input_file;
    size_t liczba_zadan;
    vector <Task> zadania;

    char * pliki[3] = {"in200.txt","in100.txt","in50.txt"};
    for(j=0;j<3;j++)
    {
        zadania.clear();
        input_file.open(pliki[j], ios::out);
        if (input_file.is_open())
        {
            input_file >> liczba_zadan;
            input_file.ignore(80,'\n');
            zadania.resize(liczba_zadan);

            for (i=0;i<liczba_zadan;i++)
            {
                zadania[i].id=i;
                input_file >> zadania[i].r;
                input_file >> zadania[i].p;
                input_file >> zadania[i].q;
            }
        input_file.close();
        }
        else
            cout << "Błąd otwierania pliku";

        algorytm_rpq(zadania);

        size_t czas=0;
        size_t czas_q=0;

        for(vector<Task>::const_iterator i=zadania.begin();i!=zadania.end();i++)
        {
            if((*i).r>czas)
                czas=(*i).r;
            czas+=(*i).p;
            if ((czas + (*i).q) > czas_q)
                czas_q=(czas + (*i).q);
        }
        czas_suma+=czas_q;
    }
    cout << czas_suma << endl;
}

